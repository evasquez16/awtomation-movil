package co.com.awto.app.actions;

import org.openqa.selenium.WebElement;

public class Is {

    public static boolean displayed(WebElement element) throws Exception {
        try{
            return element.isDisplayed();
        }catch (Exception e) {
            throw new Exception("El elemento " + element + " no es visible en el DOM " + e);
        }
    }

}
