package co.com.awto.app.actions;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Click {

    public static void on(WebDriverWait wait, WebElement element) throws Exception{
        try{
            wait.until(ExpectedConditions.elementToBeClickable(element));
            element.click();
        }catch (Exception e) {
            throw new Exception("No se pudo clickear el elemento: " + element + " " + e);
        }
    }
}
