package co.com.awto.app.actions;

import co.com.awto.app.utils.Convert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Checked {

    public static void angular(WebDriverWait wait, WebElement element, WebElement elementSelected, String option) throws Exception {
        try {
            if (elementSelected.isSelected()) {
                if (!Convert.stringToBoolean(option)) {
                    Click.on(wait, element);
                }
            } else if (elementSelected.isSelected()) {
                if (Convert.stringToBoolean(option)) {
                    Click.on(wait, element);
                }
            }
        } catch (Exception e) {
            throw new Exception("No se pudo chequear el elemento: " + element + e);
        }
    }
}
