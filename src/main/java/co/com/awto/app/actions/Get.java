package co.com.awto.app.actions;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Get {

    public static String stringText(WebDriverWait wait, WebElement element) throws Exception {
        try{
            wait.until(ExpectedConditions.visibilityOf(element));
            return element.getText();
        } catch(Exception e) {
            throw new Exception("No se pudo obtener el texto del elemento: " + element + e);
        }
    }
}
