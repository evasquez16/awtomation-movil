package co.com.awto.app.actions;

import org.openqa.selenium.WebElement;

public class Enter {

    public static void text(WebElement element, String value) throws Exception {
        try{
            element.sendKeys(value);
        }catch (Exception e) {
            throw new Exception("No se pudo escribir sobre el elemento: " + element + " " + e);
        }
    }
}
