package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class MyAccountPage extends BasePage{

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvName")
    private MobileElement userNameTxt;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvEmail")
    private MobileElement userEmailTxt;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvTitle")
    private MobileElement pageTitleTxt;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/btnBack")
    private MobileElement backButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonBack")
    private MobileElement backButton2;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonSettingPin")
    private MobileElement settingPinButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonSettingPasswordChange")
    private MobileElement passwordChangeButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etPasswordOld")
    private MobileElement inputOldPassword;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etPassword")
    private MobileElement inputNewPassword;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etPasswordConfirmation")
    private MobileElement inputConfirmNewPassword;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/btnContinue")
    private MobileElement btnContinue;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/btnSave")
    private MobileElement btnSavePassword;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/fbMessage")
    private MobileElement msgPinUpdated;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/fbMessage")
    private MobileElement msgPasswordUpdated;

    public MyAccountPage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
