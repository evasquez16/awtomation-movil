package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class MakeTripPage extends BasePage{
    public MakeTripPage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonStartTrip")
    private MobileElement openDoorsButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonCancelReservation")
    private MobileElement cancelTripButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonReport")
    private MobileElement reportingDamageButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonNoDamageReport")
    private MobileElement notReportingDamageButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonNoDamageReport")
    private MobileElement confirmNotReportingDamageButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonFinishTrip")
    private MobileElement finishTripButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonContinue")
    private MobileElement confirmFinishTripButton;

   @AndroidFindBy(xpath = "//android.widget.TextView[@text='\u00a1Excelente!']")
    private MobileElement experienceButton;

   @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonSend")
    private MobileElement sendButton;

   @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonReserveParking")
    private MobileElement reserveParkingButton;

   @AndroidFindBy(xpath = "//android.widget.TextView[@text='Centro de Movilidad AWTO']")
    private MobileElement selectParkingButton;

   @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonReserve")
    private MobileElement confirmSelectParkingButton;

   @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonFuelRecharge")
    private MobileElement rechargeFuelButton;

   @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvProviderTitle")
    private MobileElement fuelProvider;

   @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonRequestFuelRecharge")
    private MobileElement requestFuelRechargeButton;

   @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonFuelRechargeOk")
    private MobileElement returnMyTripButton;

   @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonConfirm")
    private MobileElement confirmReturnMyTripButton;

}
