package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

@Component
@Getter
public class ReserveAwtoPage extends BasePage {

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonChangeVehicle")
    private MobileElement changeVehicle;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonReserve")
    private MobileElement reserveButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonExtendedReservation")
    private MobileElement reserveExtendedButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonContinue")
    private MobileElement continueReserveExtendedButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonReserve")
    private MobileElement confirmReserveExtendedButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/mdtp_time_picker")
    private MobileElement frameReserveExtendedButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvReservationDay")
    private MobileElement dayReserveExtendedButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvReservationHour")
    private MobileElement hourReserveExtendedButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/mdtp_ok")
    private MobileElement optionOkReserveExtendedButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/layoutVehicleZone")
    private MobileElement changeZoneButton;

    public MobileElement getSelectVehicleButton(){
        return driver.findElement(By.xpath("(//android.widget.TextView[@resource-id='com.mobiag.awto.debug:id/tvPlate'])[1]"));
    }

    public MobileElement getSelectVehicleButton(String vehicle){
        return driver.findElement(By.xpath("//android.widget.TextView[@text='"+vehicle+"']"));
    }

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonFilters")
    private MobileElement filterVehicleButton;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='City Car']")
    private MobileElement filterVehicleTypeButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonApply")
    private MobileElement applyfiltersButton;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Debido a multiples cancelaciones de viaje, tu cuenta se bloqueó temporalmente']")
    private MobileElement msgSuspendAccount;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/snackbar_text")
    private MobileElement messageNotBalance;

    public ReserveAwtoPage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
