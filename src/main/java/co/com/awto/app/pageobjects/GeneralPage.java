package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class GeneralPage extends BasePage {


    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
    private MobileElement btnAcceptPermission;

    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_deny_button")
    private MobileElement btnDenyPermission;

    @AndroidFindBy(id = "com.android.permissioncontroller:id/permission_allow_one_time_button")
    private MobileElement acceptPermissionButton;

    @AndroidFindBy(id = "com.android.permissioncontroller:id/permission_allow_button")
    private MobileElement acceptAccessImageButton;

    @AndroidFindBy(id = "com.android.camera2:id/shutter_button")
    private MobileElement shutterCameraButton;

    @AndroidFindBy(id = "com.android.camera2:id/done_button")
    private MobileElement acceptPhotoButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/crop_image_menu_crop")
    private MobileElement cropPhotoButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/message")
    private MobileElement accountNotVerifyMessage;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonAccept")
    private MobileElement understandButton;


    @AndroidFindBy(id = "com.mobiag.awto.debug:id/pinKey_1")
    private MobileElement numberButton1;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/pinKey_2")
    private MobileElement numberButton2;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/pinKey_3")
    private MobileElement numberButton3;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/pinKey_4")
    private MobileElement numberButton4;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/pinKey_5")
    private MobileElement numberButton5;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/pinKey_6")
    private MobileElement numberButton6;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/pinKey_7")
    private MobileElement numberButton7;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/pinKey_8")
    private MobileElement numberButton8;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/pinKey_9")
    private MobileElement numberButton9;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/pinKey_0")
    private MobileElement numberButton0;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonCall")
    private MobileElement btnCall;

    @AndroidFindBy(id = "com.google.android.dialer:id/incall_end_call")
    private MobileElement btnEndCall;

    public GeneralPage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
