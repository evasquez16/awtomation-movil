package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class TripDetailPage extends BasePage{

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvCarPlate")
    private MobileElement vehicleTrip;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvPrice")
    private MobileElement priceTrip;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvTripAmount")
    private MobileElement tripAmount;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvTripTotal")
    private MobileElement tripTotal;

    public TripDetailPage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
