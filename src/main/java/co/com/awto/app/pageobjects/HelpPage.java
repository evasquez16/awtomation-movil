package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

@Component
@Getter
public class HelpPage extends BasePage {

    public MobileElement getOption(String option) {
        return driver.findElement(By.xpath("//android.widget.TextView[@text='" + option + "']"));
    }

    public MobileElement getDynamicOption(String option) {
        return driver.findElement(By.xpath("//android.widget.Button[@text='" + option + "']"));
    }

    @AndroidFindBy(xpath = "(//*[@class='android.view.View'])[18]")
    private MobileElement nexOption;

    public HelpPage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
