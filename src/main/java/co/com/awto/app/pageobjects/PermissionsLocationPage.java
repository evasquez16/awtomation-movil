package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class PermissionsLocationPage extends BasePage{

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonLocationPermission")
    private MobileElement acceptPermissionLocationButton;

    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
    private MobileElement allowPermissionButton;

    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_deny_button")
    private MobileElement denyPermissionButton;

    public PermissionsLocationPage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
