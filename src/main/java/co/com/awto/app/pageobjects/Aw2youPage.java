package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

@Component
@Getter
public class Aw2youPage extends BasePage {

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonOrderAw2u")
    private MobileElement orderAw2youButton;

    public Aw2youPage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
