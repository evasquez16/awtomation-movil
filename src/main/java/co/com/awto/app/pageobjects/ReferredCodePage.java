package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class ReferredCodePage extends BasePage{

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvReferralCode")
    private MobileElement referredCodeInput;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonCopy")
    private MobileElement referredCodeCopy;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonInviteFriends")
    private MobileElement referredCodeInvitedFriends;

    @AndroidFindBy(id = "android:id/content_preview_text")
    private MobileElement msgInvitedFriends;

    public ReferredCodePage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
