package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

@Component
@Getter
public class HomePage extends BasePage {

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonDrawer")
    private MobileElement drawerButton;

    @AndroidFindBy(className = "android.widget.TextView")
    private MobileElement searchInput;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etSearchBox")
    private MobileElement searchPointInput;

    //@AndroidFindBy(xpath = "//android.widget.TextView[@text='Barrio Pehuen']")
    //private MobileElement searchDirectionOption;

    //@AndroidFindBy(xpath = "//android.widget.TextView[@text='Centro de Movilidad AWTO']")
    //private MobileElement searchPointOption;

    public MobileElement getSearchDirectionPointOption(String direction){
        return driver.findElement(By.xpath("//android.widget.TextView[@text='"+direction+"']"));
    }

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonZoneDetail")
    private MobileElement viewZoneDetail;

    @AndroidFindBy(xpath = "(//*[@class='android.widget.ImageButton'])[1]")
    private MobileElement selectAwtoButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/fabFilterPremium")
    private MobileElement premiumAwtoOption;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/fabFilterSuperCargo")
    private MobileElement superCargoAwtoOption;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/fabFilterSuv")
    private MobileElement suvAwtoOption;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/fabFilterCityCar")
    private MobileElement cityCarAwtoOption;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/fabFilterCargo")
    private MobileElement cargoAwtoOption;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/fabFilterMotorcycle")
    private MobileElement motorcycleOption;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonUserLocation")
    private MobileElement userLocationButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonRefresh")
    private MobileElement refreshMapButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonMapFilter")
    private MobileElement mapFilterButton;

    public HomePage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
