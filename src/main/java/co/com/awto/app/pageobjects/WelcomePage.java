package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class WelcomePage extends BasePage {

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/ivOnboarding")
    private MobileElement welcomeImg;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/btnSignUp")
    private MobileElement createAccountButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/onboardingQuestion")
    private MobileElement onBoardingButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/btnLogin")
    private MobileElement loginButton;

    public WelcomePage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
