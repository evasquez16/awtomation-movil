package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class LoginPage extends BasePage{

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/btnCreateAccount")
    private MobileElement createAccountButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etEmail")
    private MobileElement emailInput;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etPassword")
    private MobileElement passwordInput;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/btnForgotPassword")
    private MobileElement forgotPasswordLnk;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/btnLogin")
    private MobileElement loginButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/message")
    private MobileElement revisionAccountMsg;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonAccept")
    private MobileElement msgRevisionAccountButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/btnLoginFacebook")
    private MobileElement continueWhithFacebookButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/btnLoginGoogle")
    private MobileElement continueWhithGoogleButton;

    public LoginPage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
