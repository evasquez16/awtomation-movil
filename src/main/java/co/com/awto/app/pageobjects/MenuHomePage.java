package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class MenuHomePage extends BasePage {

    @AndroidFindBy(xpath = "//*[@class='android.widget.CheckedTextView' and @text='Viajes']")
    private MobileElement tripsOption;

    @AndroidFindBy(xpath = "//*[@class='android.widget.CheckedTextView' and @text='Pagos y facturaci\u00f3n']")
    private MobileElement payBillingOption;

    @AndroidFindBy(xpath = "//*[@class='android.widget.CheckedTextView' and @text='Call Center Awto']")
    private MobileElement callCenterOption;

    @AndroidFindBy(xpath = "//*[@class='android.widget.CheckedTextView' and @text='Inbox y promociones']")
    private MobileElement inboxOption;

    @AndroidFindBy(xpath = "//*[@class='android.widget.CheckedTextView' and @text='Aw2you']")
    private MobileElement awt2YouOption;

    @AndroidFindBy(xpath = "//*[@class='android.widget.CheckedTextView' and @text='Mi c\u00f3digo referido']")
    private MobileElement myCodeOption;

    @AndroidFindBy(xpath = "//*[@class='android.widget.CheckedTextView' and @text='Ayuda']")
    private MobileElement helpOption;

    @AndroidFindBy(xpath = "//*[@class='android.widget.CheckedTextView' and @text='Mi cuenta']")
    private MobileElement myAccountOption;

    @AndroidFindBy(xpath = "//*[@class='android.widget.CheckedTextView' and @text='Cerrar sesi\u00f3n']")
    private MobileElement logOutOption;

    public MenuHomePage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
