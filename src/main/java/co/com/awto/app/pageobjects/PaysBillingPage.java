package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class PaysBillingPage extends BasePage{

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvTitle")
    private MobileElement pageTitlePayBilling;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Cargo por viaje']")
    private MobileElement burdenTrip;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvCredits")
    private MobileElement totalCredits;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Abono de cr\u00e9ditos']")
    private MobileElement paymentCredits;

    public PaysBillingPage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
