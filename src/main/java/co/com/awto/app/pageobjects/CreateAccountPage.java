package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class CreateAccountPage extends BasePage {

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/ivSignUpWelcome")
    private MobileElement singUpImg;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonNewRegister")
    private MobileElement newRegisterButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/btnLetsGo")
    private MobileElement letsGoButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etName")
    private MobileElement nameInput;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etSurname")
    private MobileElement lastNameInput;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/btnContinue")
    private MobileElement continueButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etEmail")
    private MobileElement emailInput;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etPassword")
    private MobileElement passwordInput;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etPasswordConfirmation")
    private MobileElement confirmPasswordInput;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonCreateAccount")
    private MobileElement createAccountButton;

    @AndroidFindBy(xpath = "//android.widget.RadioButton[@text='Masculino']")
    private MobileElement genderRadio;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonCountryPicker")
    private MobileElement countrySelect;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Albania']")
    private MobileElement countrySelectOption;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etPassport")
    private MobileElement passportInput;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etAddress")
    private MobileElement directionButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etSearchBox")
    private MobileElement directionInput;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/rvPlacesResult")
    private MobileElement directionOption;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonUpdateProfile")
    private MobileElement updateAccountButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etPhone")
    private MobileElement phoneNumberInput;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonUpdatePhoneNumber")
    private MobileElement updatePhoneNumberButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/btnAccept")
    private MobileElement acceptButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/ivVehicleOptionBoth")
    private MobileElement bothVehiclesButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonUpdateVehicleType")
    private MobileElement updateVehicleTypeButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonPickImage")
    private MobileElement pickImageButton;

    @AndroidFindBy(id = "com.android.permissioncontroller:id/permission_allow_one_time_button")
    private MobileElement acceptRecordVideo;

    @AndroidFindBy(id = "com.android.permissioncontroller:id/permission_allow_button")
    private MobileElement acceptPhonoMedia;

    @AndroidFindBy(xpath = "(//android.widget.TextView)[3]")
    private MobileElement memberShipButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonUpdateMembership")
    private MobileElement updateMemberShipButton;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='No tengo tarjeta de cr\u00e9dito']")
    private MobileElement optionPay;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/btnContinueUpdateMembership")
    private MobileElement continueUpdateMemberShipButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/ivSignUpWelcome")
    private MobileElement congratulationsImg;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonCancel")
    private MobileElement inOtherMomentButon;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonContinue")
    private MobileElement continueRegisterButton;

    public CreateAccountPage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
