package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class InboxPromosPage extends BasePage {

    @AndroidFindBy(xpath = "//androidx.appcompat.app.ActionBar.Tab[@content-desc='Promociones']")
    private MobileElement tabPromociones;

    @AndroidFindBy(xpath = "//androidx.appcompat.app.ActionBar.Tab[@content-desc='Inbox']")
    private MobileElement tabInbox;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvNoItems")
    private MobileElement noItems;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonEnterCode")
    private MobileElement enterCodeButton;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/etCode")
    private MobileElement enterCodeInput;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/buttonSendCode")
    private MobileElement sendCodeButton;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='C\u00f3digo de cup\u00f3n inv\u00e1lido']")
    private MobileElement codeInvalid;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='La promoci\u00f3n no est\u00e1 disponible']")
    private MobileElement codeNotAvailable;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Este cup\u00f3n es valido solo durante el registro']")
    private MobileElement codeRegisterOnly;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/fbMessage")
    private MobileElement codeApplied;

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/snackbar_text")
    private MobileElement msgPresent;


    public InboxPromosPage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
