package co.com.awto.app.pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class TripsPage extends BasePage{

    @AndroidFindBy(id = "com.mobiag.awto.debug:id/tvTitle")
    private MobileElement pageTitleTrips;

    @AndroidFindBy(xpath = "(//android.widget.TextView[@text])[2]")
    private MobileElement fisrtTrip;

    public TripsPage(AppiumDriver<MobileElement> driver) {
        super(driver);
    }
}
