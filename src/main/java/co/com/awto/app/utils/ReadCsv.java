package co.com.awto.app.utils;

import com.opencsv.CSVReader;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Component
public class ReadCsv {

    private String[] row = null;
    private CSVReader reader = null;

    public String[] getDataRow(int column, String valueCompare, String archive) throws IOException {
        getAllData(archive);
        while ((row = this.reader.readNext()) != null)
            if (row[column].equals(valueCompare))
                break;
        return row;
    }

    public List<String[]> getDataGroupRow(int column, String valueCompare, String archive) throws IOException {
        getAllData(archive);
        List<String[]> rowList = new ArrayList<String[]>();
        while ((row = this.reader.readNext()) != null)
            if (row[column].equals(valueCompare))
                rowList.add(row);
        return rowList;
    }

    public CSVReader getAllData(String archive) throws IOException {
        return this.reader = new CSVReader(new InputStreamReader(new FileInputStream(archive), "UTF-8"), '|');
    }
}
