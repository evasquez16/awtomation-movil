package co.com.awto.app.utils;

public class Convert {

    public static boolean stringToBoolean(String value){
        boolean flag;
        switch (value.toUpperCase()){
            case "SI":
            case "TRUE":
                flag = true;
                break;
            case "NO":
            case "FALSE":
                flag = false;
                break;
            default:
                throw new IllegalArgumentException("Invalid option " + value);
        }
        return flag;
    }
}
