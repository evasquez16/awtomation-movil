package co.com.awto.app.services;

import co.com.awto.app.pageobjects.InboxPromosPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InboxPromosServices extends BaseServices {

    @Autowired
    private InboxPromosPage inboxPromosPage;

    public void clickOnTabInbox() throws Exception {
        this.click(inboxPromosPage.getTabInbox());
    }

    public Boolean msgNoItemsPresent() throws Exception {
        return isElementPresent(inboxPromosPage.getNoItems());
    }

    public void clickOnTabPromos() throws Exception {
        this.click(inboxPromosPage.getTabPromociones());
    }

    public void clickOnEnterCode() throws Exception {
        this.click(inboxPromosPage.getEnterCodeButton());
    }

    public void enterValueCodePromo(String code) throws Exception {
        clickOnEnterCode();
        this.enterTheValue(inboxPromosPage.getEnterCodeInput(), code);
        this.click(inboxPromosPage.getSendCodeButton());
    }

    public Boolean validateMessagePresent(String messageType) throws Exception {
        boolean flag = false;
        switch (messageType) {
            case "valida":
                flag = isElementPresent(inboxPromosPage.getCodeApplied());
                break;
            case "incorrecta":
                flag = isElementPresent(inboxPromosPage.getCodeInvalid());
                break;
            case "solo registro":
                flag = isElementPresent(inboxPromosPage.getCodeRegisterOnly());
                break;
            case "repetida":
            case "caducada":
                flag = isElementPresent(inboxPromosPage.getCodeNotAvailable());
                break;
        }
        return flag;
    }
}
