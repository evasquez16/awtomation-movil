package co.com.awto.app.services;

import co.com.awto.app.pageobjects.ReferredCodePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReferredCodeServices extends BaseServices{

    @Autowired
    private ReferredCodePage referredCodePage;

    public String getCodeReferred() throws Exception {
        return this.getText(referredCodePage.getReferredCodeInput());
    }

    public void clickOnCopyButton() throws Exception {
        this.click(referredCodePage.getReferredCodeCopy());
        driver.navigate().back();
    }

    public void clickOnInvitedFriendsButton() throws Exception {
        this.click(referredCodePage.getReferredCodeInvitedFriends());
    }

    public Boolean validateMsgPresent() throws Exception {
        clickOnInvitedFriendsButton();
        return this.isElementPresent(referredCodePage.getMsgInvitedFriends());
    }
}
