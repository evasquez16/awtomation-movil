package co.com.awto.app.services;

import co.com.awto.app.models.UserModel;
import co.com.awto.app.pageobjects.LoginPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginServices extends BaseServices {

    @Autowired
    public LoginPage loginPage;

    public void enterUserAndPassword(UserModel userModel) throws Exception {
        enterUser(userModel.getEmail());
        enterPassword(userModel.getPassWord());
        clickOnLoginButton();
    }

    public void enterUser(String email) throws Exception {
        this.enterTheValue(loginPage.getEmailInput(),email);
    }

    public void enterPassword(String password) throws Exception {
        this.enterTheValue(loginPage.getPasswordInput(),password);
    }

    public void clickOnLoginButton() throws Exception {
        this.click(loginPage.getLoginButton());
    }

    public Boolean viewMsgRevisionAccount() throws Exception {
        return isElementPresent(loginPage.getRevisionAccountMsg());
    }
    public void clickOnButtonAcceptRevisionAccount() throws Exception {
        this.click(loginPage.getMsgRevisionAccountButton());
        driver.navigate().back();
    }
}
