package co.com.awto.app.services;

import co.com.awto.app.pageobjects.WelcomePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WelcomeServices extends BaseServices {
    @Autowired
    private WelcomePage welcomePage;

    public void waitForWelcomePage() throws Exception {
        waitForElementPresent(welcomePage.getWelcomeImg());
    }

    public boolean verifyImgWelcomePagePresent() throws Exception {
        return isElementPresent(welcomePage.getWelcomeImg());
    }

    public void clickCreateAccountButton() throws Exception {
        this.click(welcomePage.getCreateAccountButton());
    }

    public void clickOnBoardingButton() throws Exception {
        this.click(welcomePage.getOnBoardingButton());
    }

    public void clickLoginButton() throws Exception {
        this.click(welcomePage.getLoginButton());
    }
}
