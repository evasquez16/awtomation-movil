package co.com.awto.app.services;

import co.com.awto.app.pageobjects.PaysBillingPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PaysBillingServices extends BaseServices {

    @Autowired
    private PaysBillingPage paysBillingPage;

    public String getTitlePage() throws Exception {
        return this.getText(paysBillingPage.getPageTitlePayBilling());
    }

    public Boolean burdenTrip() throws Exception {
        return this.isElementPresent(paysBillingPage.getBurdenTrip());
    }

    public Boolean totalCredits() throws Exception {
        return this.isElementPresent(paysBillingPage.getTotalCredits());
    }

    public Boolean paymentCredits() throws Exception {
        return this.isElementPresent(paysBillingPage.getPaymentCredits());
    }
}
