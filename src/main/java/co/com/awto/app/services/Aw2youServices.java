package co.com.awto.app.services;

import co.com.awto.app.actions.Click;
import co.com.awto.app.pageobjects.Aw2youPage;
import co.com.awto.app.pageobjects.GeneralPage;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Aw2youServices {

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private Aw2youPage aw2you;

    @Autowired
    private GeneralPage general;

    public void clickOnAcceptPermission() throws Exception {
        Click.on(wait, general.getBtnAcceptPermission());
    }

    public void clickOnCall() throws Exception {
        Click.on(wait, general.getBtnCall());
        Thread.sleep(5000);
    }

    public void clickOnEndCall() throws Exception {
        Click.on(wait, general.getBtnEndCall());
    }

    public void clickOnOrderAw2you() throws Exception {
        Click.on(wait, aw2you.getOrderAw2youButton());
    }
}
