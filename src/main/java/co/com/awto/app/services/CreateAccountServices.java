package co.com.awto.app.services;

import co.com.awto.app.models.ClientModel;
import co.com.awto.app.pageobjects.CreateAccountPage;
import co.com.awto.app.pageobjects.GeneralPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CreateAccountServices extends BaseServices {

    @Autowired
    private CreateAccountPage createAccountPage;
    @Autowired
    private GeneralPage generalPage;
    @Autowired
    private AppiumDriver<MobileElement> driver;


    public void registerClientFields(ClientModel clientModel) throws Exception {
        //if (isElementPresent(createAccountPageObjects.newRegisterButton))
        //    clickOnNewRegisterButton();
        clickOnLetsGoButton();
        enterFullName(clientModel.getName(), clientModel.getLastName());
        enterEmailPassword(clientModel.getEmail(), clientModel.getPassword());
        selectGenderCountryDirection();
        enterRutDirection(clientModel.getPassport(), clientModel.getDirection());
        enterPhoneNumber(clientModel.getPhoneNumber());
        clickOnTypeVehicles();
        documentsImagesChargue();
        selfieImageChargue();
        selectMembership();
        selectOptionPay();
    }

    public boolean verifySingUpImg() throws Exception {
        return isElementPresent(createAccountPage.getSingUpImg());
    }

    public void clickOnNewRegisterButton() {
        createAccountPage.getNewRegisterButton().click();
    }

    public void clickOnLetsGoButton() {
        createAccountPage.getLetsGoButton().click();
    }

    public void enterFullName(String name, String lastName) {
        createAccountPage.getNameInput().sendKeys(name);
        createAccountPage.getLastNameInput().sendKeys(lastName);
        createAccountPage.getContinueButton().click();
    }

    public void enterEmailPassword(String email, String password) {
        createAccountPage.getEmailInput().sendKeys(email);
        createAccountPage.getPasswordInput().sendKeys(password);
        createAccountPage.getConfirmPasswordInput().sendKeys(password);
        driver.hideKeyboard();
        createAccountPage.getCreateAccountButton().click();
    }

    public void selectGenderCountryDirection() {
        createAccountPage.getGenderRadio().click();
        createAccountPage.getCountrySelect().click();
        createAccountPage.getCountrySelectOption().click();
    }

    public void enterRutDirection(String passport, String direction) {
        createAccountPage.getPassportInput().sendKeys(passport);
        createAccountPage.getDirectionButton().click();
        createAccountPage.getDirectionInput().sendKeys(direction);
        createAccountPage.getDirectionOption().click();
        createAccountPage.getUpdateAccountButton().click();
    }

    public void enterPhoneNumber(String phoneNumber) {
        createAccountPage.getPhoneNumberInput().sendKeys(phoneNumber);
        createAccountPage.getUpdatePhoneNumberButton().click();
    }

    public void clickOnTypeVehicles() throws Exception {
        if (isElementPresent(createAccountPage.getAcceptButton()))
            createAccountPage.getAcceptButton().click();
        createAccountPage.getBothVehiclesButton().click();
        createAccountPage.getUpdateVehicleTypeButton().click();
    }

    public void documentsImagesChargue() {
        try {
            for (int x = 1; x <= 4; x++) {
                clickOnPickImageButton();
                if (x == 1) {
                    createAccountPage.getAcceptRecordVideo().click();
                    createAccountPage.getAcceptPhonoMedia().click();
                }
                Thread.sleep(5000);
                generalPage.getShutterCameraButton().click();
                generalPage.getAcceptPhotoButton().click();
                generalPage.getCropPhotoButton().click();
                clickOnLetsGoButton();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void selfieImageChargue() throws IOException {
        try {
            clickOnPickImageButton();
            Thread.sleep(5000);
            generalPage.getShutterCameraButton().click();
            generalPage.getAcceptPhotoButton().click();
            clickOnLetsGoButton();
            createAccountPage.getAcceptButton().click();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickOnPickImageButton() {
        createAccountPage.getPickImageButton().click();
    }

    public void selectMembership() {
        createAccountPage.getMemberShipButton().click();
        createAccountPage.getUpdateMemberShipButton().click();
    }

    public void selectOptionPay() {
        createAccountPage.getOptionPay().click();
        createAccountPage.getContinueUpdateMemberShipButton().click();
    }

    public boolean validateAccountCreate() throws Exception {
        return isElementPresent(createAccountPage.getCongratulationsImg());
    }

    public void clickOnUnderstandButton() {
        createAccountPage.getAcceptButton().click();
    }

    public void clickOnInOtherMomentButton() {
        createAccountPage.getInOtherMomentButon().click();
    }

    public void clickOnContinueRegisterButton() {
        createAccountPage.getContinueRegisterButton().click();
    }

}
