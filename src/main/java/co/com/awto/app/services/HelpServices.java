package co.com.awto.app.services;

import co.com.awto.app.actions.Click;
import co.com.awto.app.pageobjects.GeneralPage;
import co.com.awto.app.pageobjects.HelpPage;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HelpServices extends BaseServices {

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private HelpPage help;

    @Autowired
    private GeneralPage general;

    public void clickOnOption(String option) throws Exception {
        Click.on(wait, help.getOption(option));
    }

    public void clickOnAcceptPermission() throws Exception {
        if (isElementPresent(general.getBtnAcceptPermission()))
            Click.on(wait, general.getBtnAcceptPermission());
    }

    public void clickOnCall() throws Exception {
        Click.on(wait, general.getBtnCall());
        Thread.sleep(5000);
    }

    public void clickOnEndCall() throws Exception {
        Click.on(wait, general.getBtnEndCall());
    }

    public void clickOnDinamicOption(String option) throws Exception {
        switch (option) {
            case "FAQ's":
                Click.on(wait, help.getOption("¿C\u00f3mo cargar bencina?"));
                break;
            case "Tutorial Awto":
                for (int x = 0; x <= 3; x++)
                    if (isElementPresent(help.getNexOption()))
                        Click.on(wait, help.getNexOption());
                Thread.sleep(500);
                break;
            case "Términos y Condiciones":
                Click.on(wait, help.getDynamicOption("1.- Antecedentes "));
                break;
        }
    }
}
