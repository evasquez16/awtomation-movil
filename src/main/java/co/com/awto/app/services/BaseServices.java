package co.com.awto.app.services;

import co.com.awto.app.pageobjects.GeneralPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
public class BaseServices {

    @Autowired
    private WebDriverWait wait;
    @Autowired
    private GeneralPage generalPage;
    @Autowired
    public AppiumDriver<MobileElement> driver;

    public void waitForElementPresent(WebElement element) throws Exception {
        try {
            Thread.sleep(1000);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (Exception e) {
            throw new Exception("No se puede localizar el elemento: " + element + " " + e);
        }
    }

    public void click(WebElement element) throws Exception {
        try {
            waitForElementPresent(element);
            element.click();
        } catch (Exception e) {
            throw new Exception("No se puede clickear el elemento: " + element + " " + e);
        }
    }

    public void clickInPoint(WebElement element, int x, int y) throws Exception {
        try {
            waitForElementPresent(element);
            new TouchAction<>(driver).tap(PointOption.point(x, y)).release().perform();
        } catch (Exception e) {
            throw new Exception("No se puede clickear el elemento: " + element + " " + e);
        }
    }

    public void longPressPoint(WebElement element, int x, int y) throws Exception {
        try {
            waitForElementPresent(element);
            click(element);
            new TouchAction<>(driver).longPress(PointOption.point(x, y)).release().perform();
        } catch (Exception e) {
            throw new Exception("No se puede clickear sostenidamente el elemento: " + element + " " + e);
        }
    }

    public void enterTheValue(WebElement element, String value) throws Exception {
        try {
            waitForElementPresent(element);
            element.sendKeys(value);
        } catch (Exception e) {
            throw new Exception("No se pudo diligenciar el elemento: " + element + " " + e);
        }
    }

    public String getText(WebElement element) throws Exception {
        try {
            waitForElementPresent(element);
            return element.getText().trim();
        } catch (NoSuchElementException e) {
            throw new Exception("No se puede extraer el texto del elemento: " + element + " " + e);
        }
    }

    public boolean isElementPresent(WebElement element) throws Exception {
        try {
            element.isDisplayed();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void enterPin(String pin) {
        WebElement numberSelect;
        for (int x = 0; x <= 3; x++) {
            switch (pin.trim().charAt(x)) {
                case '1':
                    numberSelect = generalPage.getNumberButton1();
                    break;
                case '2':
                    numberSelect = generalPage.getNumberButton2();
                    break;
                case '3':
                    numberSelect = generalPage.getNumberButton3();
                    break;
                case '4':
                    numberSelect = generalPage.getNumberButton4();
                    break;
                case '5':
                    numberSelect = generalPage.getNumberButton5();
                    break;
                case '6':
                    numberSelect = generalPage.getNumberButton6();
                    break;
                case '7':
                    numberSelect = generalPage.getNumberButton7();
                    break;
                case '8':
                    numberSelect = generalPage.getNumberButton8();
                    break;
                case '9':
                    numberSelect = generalPage.getNumberButton9();
                    break;
                default:
                    numberSelect = generalPage.getNumberButton0();
                    break;
            }
            numberSelect.click();
        }
    }
    
    public void scrollDown() {
        Dimension size = driver.manage().window().getSize();
        int width = size.width / 2;
        int startPoint = (int) (size.getHeight() * 0.80);
        int endPoint = (int) (size.getHeight() * 0.20);
        new TouchAction(driver).press(PointOption.point(width, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000))).moveTo(PointOption.point(width, endPoint)).release().perform();
    }

    public void goToBack() {
        driver.navigate().back();
    }
}
