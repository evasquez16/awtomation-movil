package co.com.awto.app.services;

import co.com.awto.app.pageobjects.HomePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HomeServices extends BaseServices {

    @Autowired
    private HomePage homePage;
    private final String point = "el punto";
    private final String direction = "la direccion";

    public void waitLoadingHomePage() throws Exception {
        waitForElementPresent(homePage.getDrawerButton());
    }

    public void clickOnDrawerButton() throws Exception {
        if (isElementPresent(homePage.getDrawerButton()))
            this.click(homePage.getDrawerButton());
    }

    public void clickOnVehiclesOption() throws Exception {
        Thread.sleep(1000);
        this.click(homePage.getSelectAwtoButton());
    }

    public void clickOnAwtoOption(String tipeVehicle) throws Exception {
        clickOnVehiclesOption();
        switch (tipeVehicle) {
            case "Premium":
                this.click(homePage.getPremiumAwtoOption());
                break;
            case "Super cargo":
                this.click(homePage.getSuperCargoAwtoOption());
                break;
            case "SUV":
                this.click(homePage.getSuvAwtoOption());
                break;
            case "Cargo":
                this.click(homePage.getCargoAwtoOption());
                break;
            case "Moto":
                this.click(homePage.getMotorcycleOption());
                break;
            default:
            case "Citycar":
                this.click(homePage.getCityCarAwtoOption());
                break;
        }
    }

    public void enterValueSearchDirectionPoint(String value) throws Exception {
        Thread.sleep(2000);
        this.click(homePage.getSearchInput());
        this.enterTheValue(homePage.getSearchPointInput(), value);
    }

    public void pasteValueSearchDirectionPoint(String value) throws Exception {
        Thread.sleep(2000);
        this.click(homePage.getSearchInput());
        this.longPressPoint(homePage.getSearchPointInput(), 200, 540);
        this.enterTheValue(homePage.getSearchPointInput(), value);
    }

    public boolean validateDirectionPointsPresent(String value) throws Exception {
        return isElementPresent(homePage.getSearchDirectionPointOption(value));
    }

    public void clickOnSearchPointDirectionOption(String typeSearch, String value) throws Exception {
        if (typeSearch.equals(point)) {
            this.click(homePage.getSearchDirectionPointOption(value));
            this.click(homePage.getViewZoneDetail());
        } else if (typeSearch.equals(direction)) {
            this.click(homePage.getSearchDirectionPointOption(value));
        }
    }
}
