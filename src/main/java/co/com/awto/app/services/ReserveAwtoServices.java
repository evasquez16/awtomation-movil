package co.com.awto.app.services;

import co.com.awto.app.pageobjects.ReserveAwtoPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReserveAwtoServices extends BaseServices {

    @Autowired
    private ReserveAwtoPage reserveAwtoPage;

    public void clickOnChangeVehicle() throws Exception {
        this.click(reserveAwtoPage.getChangeVehicle());
    }

    public void clickOnReserveButton() throws Exception {
        this.click(reserveAwtoPage.getReserveButton());
    }

    public void clickOnReserveExtendedButton() throws Exception {
        this.click(reserveAwtoPage.getReserveExtendedButton());
    }

    public void clickOnContinueReserveExtendedButton() throws Exception {
        this.click(reserveAwtoPage.getContinueReserveExtendedButton());
    }

    public void clickOnConfirmReserveExtendedButton() throws Exception {
        this.click(reserveAwtoPage.getConfirmReserveExtendedButton());
        this.click(reserveAwtoPage.getContinueReserveExtendedButton());
    }

    public void clickOnDayReserveButton() throws Exception {
        this.click(reserveAwtoPage.getDayReserveExtendedButton());
    }

    public void clickOnHourReserveButton() throws Exception {
        this.click(reserveAwtoPage.getHourReserveExtendedButton());
        this.clickInPoint(reserveAwtoPage.getFrameReserveExtendedButton(), 722, 1782);
    }

    public void clickOnOptionOkButton() throws Exception {
        this.click(reserveAwtoPage.getOptionOkReserveExtendedButton());
    }

    public void clickOnChangeZoneButton() throws Exception {
        this.click(reserveAwtoPage.getChangeZoneButton());
    }

    public void selectExtendsReserve(String pin) throws Exception {
        this.clickOnReserveExtendedButton();
        this.clickOnDayReserveButton();
        this.clickOnOptionOkButton();
        this.clickOnHourReserveButton();
        this.clickOnOptionOkButton();
        this.clickOnContinueReserveExtendedButton();
        this.clickOnConfirmReserveExtendedButton();
        this.enterPin(pin);
    }

    public void selectVehicle(String awto) throws Exception {
        this.clickOnChangeVehicle();
        this.filterVehicles();
        this.click(reserveAwtoPage.getSelectVehicleButton());
    }

    public void filterVehicles() throws Exception {
        this.click(reserveAwtoPage.getFilterVehicleButton());
        this.click(reserveAwtoPage.getFilterVehicleTypeButton());
        this.click(reserveAwtoPage.getApplyfiltersButton());
    }

    public void reserveVehicle(String pin) throws Exception {
        this.clickOnReserveButton();
        this.enterPin(pin);
        Thread.sleep(5000);
    }

    public boolean verifyNotReserveActive() throws Exception {
        return isElementPresent(reserveAwtoPage.getReserveButton());
    }

    public boolean verifyMessageNotBalance() throws Exception {
        return isElementPresent(reserveAwtoPage.getMessageNotBalance());
    }

    public boolean verifySuspendAccount() throws Exception {
        return isElementPresent(reserveAwtoPage.getMsgSuspendAccount());
    }
}
