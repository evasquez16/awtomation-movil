package co.com.awto.app.services;

import co.com.awto.app.pageobjects.TripDetailPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TripDetailServices extends BaseServices {

    @Autowired
    private TripDetailPage tripDetailPage;

    public Boolean validateDetailPresent() throws Exception {
        return isElementPresent(tripDetailPage.getVehicleTrip()) &&
                isElementPresent(tripDetailPage.getPriceTrip()) &&
                isElementPresent(tripDetailPage.getTripAmount()) &&
                isElementPresent(tripDetailPage.getTripTotal());
    }
}
