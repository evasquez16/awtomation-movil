package co.com.awto.app.services;

import co.com.awto.app.pageobjects.MenuHomePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MenuHomeServices extends BaseServices {

    @Autowired
    private MenuHomePage menuHomePage;

    public void clickOnMyAccount() throws Exception {
        this.click(menuHomePage.getMyAccountOption());
    }

    public void clickOnLogOutOption() throws Exception {
        if (isElementPresent(menuHomePage.getLogOutOption()))
            this.click(menuHomePage.getLogOutOption());
    }

    public void clickOnTripsOption() throws Exception {
        this.click(menuHomePage.getTripsOption());
    }

    public void clickOnPayVouchersOption() throws Exception {
        this.click(menuHomePage.getPayBillingOption());
    }

    public void clickOnCallCenterOption() throws Exception {
        this.click(menuHomePage.getCallCenterOption());
    }

    public void clickOnInboxOption() throws Exception {
        this.click(menuHomePage.getInboxOption());
    }

    public void clickOnAwt2YouOption() throws Exception {
        this.click(menuHomePage.getAwt2YouOption());
    }

    public void clickOnMyCodeOption() throws Exception {
        this.click(menuHomePage.getMyCodeOption());
    }

    public void clickOnHelpOption() throws Exception {
        this.click(menuHomePage.getHelpOption());
    }
}
