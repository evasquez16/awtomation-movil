package co.com.awto.app.services;

import co.com.awto.app.pageobjects.PermissionsLocationPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PermissionLocationServices extends BaseServices {

    @Autowired
    public PermissionsLocationPage permissionsLocationPage;

    public void acceptPermissionLocation() throws Exception {
        if (isElementPresent(permissionsLocationPage.getAcceptPermissionLocationButton())) {
            permissionsLocationPage.getAcceptPermissionLocationButton().click();
            permissionsLocationPage.getAllowPermissionButton().click();
        }
    }
}
