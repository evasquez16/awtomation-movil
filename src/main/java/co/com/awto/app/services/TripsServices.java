package co.com.awto.app.services;

import co.com.awto.app.pageobjects.TripsPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TripsServices extends BaseServices {

    @Autowired
    private TripsPage tripsPage;

    public String getTitlePage() {
        return tripsPage.getPageTitleTrips().getText();
    }

    public void clickOnFirstTrip() throws Exception {
        this.click(tripsPage.getFisrtTrip());
    }

}
