package co.com.awto.app.services;

import co.com.awto.app.pageobjects.GeneralPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GeneralServices extends BaseServices {

    @Autowired
    private GeneralPage generalPage;

    public void clickOnPermissionButton() {
        generalPage.getAcceptPermissionButton().click();
    }

    public void clickOnAccessImageButton() {
        generalPage.getAcceptAccessImageButton().click();
    }

    public void clickOnShutterCameraButton() {
        generalPage.getShutterCameraButton().click();
    }

    public void clickOnAcceptPhonoButton() throws Exception {
        waitForElementPresent(generalPage.getAcceptPhotoButton());
        generalPage.getAcceptPhotoButton().click();
    }

    public void clickOnCropPhotoButton() {
        generalPage.getCropPhotoButton().click();
    }

    public void verifyMessageAccount() {
        generalPage.getAccountNotVerifyMessage().click();
    }

    public void clickOnUnderstandButton() {
        generalPage.getUnderstandButton().click();
    }


}
