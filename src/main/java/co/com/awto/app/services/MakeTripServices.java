package co.com.awto.app.services;

import co.com.awto.app.pageobjects.MakeTripPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MakeTripServices extends BaseServices {

    @Autowired
    private MakeTripPage makeTripPage;

    public void clickOnOpenDoorsButton() throws Exception {
        this.click(makeTripPage.getOpenDoorsButton());
    }

    public void clickOnCancelTripButton() throws Exception {
        this.click(makeTripPage.getCancelTripButton());
    }

    public void clickOnReportingDamageButton() throws Exception {
        this.click(makeTripPage.getReportingDamageButton());
    }

    public void notReportingDamage() throws Exception {
        this.click(makeTripPage.getNotReportingDamageButton());
        this.click(makeTripPage.getConfirmNotReportingDamageButton());
    }

    public void finishTrip() throws Exception {
        this.click(makeTripPage.getFinishTripButton());
        this.click(makeTripPage.getConfirmFinishTripButton());
    }

    public void sendExperience() throws Exception {
        this.click(makeTripPage.getExperienceButton());
        this.click(makeTripPage.getSendButton());
    }

    public void reserveParking() throws Exception {
        this.click(makeTripPage.getReserveParkingButton());
        this.click(makeTripPage.getSelectParkingButton());
        this.click(makeTripPage.getConfirmSelectParkingButton());
    }

    public void startEndTrip() throws Exception {
        clickOnOpenDoorsButton();
        notReportingDamage();
        Thread.sleep(5000);
        reserveParking();
        finishTrip();
        sendExperience();
    }

    public void onlyStartTrip() throws Exception {
        clickOnOpenDoorsButton();
        notReportingDamage();
        Thread.sleep(5000);
        //reserveParking();
    }

    public void cancelTrip() throws Exception {
        clickOnCancelTripButton();
    }

    public void rechargeFuel() throws Exception {
        this.clickOnRechargeFuel();
        this.clickOnProviderFuel();
        this.clickOnRequestFuelRecharge();
        this.clickOnReturnMyTrip();
        this.clickOnConfirmReturnMyTrip();
    }

    public void clickOnRechargeFuel() throws Exception {
        this.click(makeTripPage.getRechargeFuelButton());
    }

    public void clickOnProviderFuel() throws Exception {
        this.click(makeTripPage.getFuelProvider());
    }

    public void clickOnRequestFuelRecharge() throws Exception {
        this.click(makeTripPage.getRequestFuelRechargeButton());
    }

    public void clickOnReturnMyTrip() throws Exception {
        this.click(makeTripPage.getReturnMyTripButton());
    }

    public void clickOnConfirmReturnMyTrip() throws Exception {
        this.click(makeTripPage.getConfirmReturnMyTripButton());
    }

}
