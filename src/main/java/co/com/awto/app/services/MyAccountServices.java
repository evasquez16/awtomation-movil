package co.com.awto.app.services;

import co.com.awto.app.models.UserModel;
import co.com.awto.app.pageobjects.MyAccountPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyAccountServices extends BaseServices {

    @Autowired
    private MyAccountPage myAccountPage;

    public String getNameUser() throws Exception {
        return this.getText(myAccountPage.getUserNameTxt());
    }

    public String getEmailUser() throws Exception {
        return this.getText(myAccountPage.getUserEmailTxt());
    }

    public void clickBackButton() throws Exception {
        Thread.sleep(1000);
        if (isElementPresent(myAccountPage.getBackButton()))
            this.click(myAccountPage.getBackButton());
        else if (isElementPresent(myAccountPage.getBackButton2()))
            this.click(myAccountPage.getBackButton2());
    }

    public void clickSettingPinButton() throws Exception {
        this.click(myAccountPage.getSettingPinButton());
    }

    public void clickPasswordChangeButton() throws Exception {
        this.click(myAccountPage.getPasswordChangeButton());
    }

    public void enterOldPassword(String oldPassword) throws Exception {
        this.enterTheValue(myAccountPage.getInputOldPassword(),oldPassword);
        this.click(myAccountPage.getBtnContinue());
    }

    public void enterNewPassword(String newPassword) throws Exception {
        this.enterTheValue(myAccountPage.getInputNewPassword(),newPassword);
        this.enterTheValue(myAccountPage.getInputConfirmNewPassword(),newPassword);
        this.click(myAccountPage.getBtnSavePassword());
    }

    public boolean verifyUserLogin(UserModel userModel) throws Exception {
        waitForElementPresent(myAccountPage.getPageTitleTxt());
        return getNameUser().trim().equals(userModel.getName()) &&
                getEmailUser().trim().equals(userModel.getEmail());
    }

    public void enterUserPin(String pin) {
        this.enterPin(pin);
    }

    public boolean msgPinUpdatePresent() throws Exception {
        return isElementPresent(myAccountPage.getMsgPinUpdated());
    }

    public boolean msgPasswordUpdatePresent() throws Exception {
        return isElementPresent(myAccountPage.getMsgPasswordUpdated());
    }
}
