package co.com.awto.app.models;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;
@Component
@Getter
public class ClientModel {

    private String name;
    private String lastName;
    private String fullName;
    private String email;
    private String password;
    private String gender;
    private String country;
    private String passport;
    private String direction;
    private String phoneNumber;
    private String typeVehicle;
    private String membership;
    private String codDiscount;
    private String optionPay;

    @Autowired
    public ClientModel() {

    }

    public void clientModel(List<String> data) {
        int adicion = new Random().nextInt(10000);
        this.name = data.get(0) + adicion;
        this.lastName = data.get(1);
        this.fullName = name + " " + lastName;
        this.email = data.get(2) + adicion + "@gmail.comtest";
        this.password = data.get(3);
        this.gender = data.get(4);
        this.country = data.get(5);
        this.passport = data.get(6) + adicion;
        this.direction = data.get(7);
        this.phoneNumber = data.get(8) + adicion;
        this.typeVehicle = data.get(9);
        this.membership = data.get(10);
        this.codDiscount = data.get(11);
        this.optionPay = data.get(12);
    }
}
