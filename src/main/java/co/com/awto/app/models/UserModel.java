package co.com.awto.app.models;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@Getter
public class UserModel {

    private String email;
    private String passWord;
    private String name;

    @Autowired
    public UserModel(){

    }

    public void userModel(String email, String password, String name) {
        this.email = email;
        this.passWord = password;
        this.name = name;
    }

    public void userModel(List<String> data) {
        this.email = data.get(0);
        this.passWord = data.get(1);
        this.name = data.get(2);
    }
}
