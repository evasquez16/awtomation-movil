package co.com.awto.app.config;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Configuration
@ComponentScan(basePackages = "co.com.awto.app")
@PropertySource("classpath:/application.properties")
public class DriverConfig {

    @Value("${element.wait}")
    private int waitTimeOut;
    @Autowired
    private DriverFactory driverFactory;

    @Bean(destroyMethod = "quit")
    public AppiumDriver<MobileElement> getDriver() throws IOException {
        AppiumDriver<MobileElement> driver = driverFactory.getDriver();
        driver.manage().timeouts().implicitlyWait(waitTimeOut, TimeUnit.SECONDS);
        return driver;
    }

    @Bean
    public WebDriverWait webDriverWait() throws IOException {
        return new WebDriverWait(getDriver(), waitTimeOut);
    }


}
