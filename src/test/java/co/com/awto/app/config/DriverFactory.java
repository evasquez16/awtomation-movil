package co.com.awto.app.config;

import co.com.awto.app.enums.OS;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

@Component
public class DriverFactory {

    @Value("${device.os}")
    private OS os;
    @Value("${url.driver}")
    private String urlDriver;
    @Value("${device.name}")
    private String deviceName;
    @Value("${app.version}")
    private String appVersion;
    @Value("${version.android}")
    private String versionAndroid;
    @Value("${url.app.ios}")
    private String urlAppIOS;
    @Value("${version.ios}")
    private String platformVersionIos;
    @Value("${service.driver}")
    private String serviceDriver;

    private static final DesiredCapabilities device = new DesiredCapabilities();
    private static AppiumDriver<MobileElement> driver;

    public AppiumDriver<MobileElement> getDriver() throws IOException {
        if (OS.android == os && driver == null)
            androidSetup();
        if (OS.ios == os && driver == null)
            iosSetup();
        return driver;
    }

    public void androidSetup() throws MalformedURLException {
        device.setCapability("deviceName", deviceName);
        device.setCapability("platformVersion", versionAndroid);
        device.setCapability("platformName", "Android");
        device.setCapability("app", System.getProperty("user.dir") + "/build/AwtoAppSomosnadaV." + appVersion + ".apk");
        //caps.setCapability("appPackage", "com.awtoApp");
        //caps.setCapability("appActivity", ".MainActivity");
        driver = new AppiumDriver<MobileElement>(URI.create(urlDriver).toURL(), device);
    }

    public void iosSetup() throws MalformedURLException {
        File classpathRoot = new File(System.getProperty("user.dir"));
        File appDir = new File(classpathRoot, "/build/");
        File app = new File(appDir, urlAppIOS);
        device.setCapability("app", app.getAbsolutePath());
        device.setCapability("deviceName", deviceName);
        device.setCapability("platformName", "IOS");
        device.setCapability("platformVersion", platformVersionIos);
        driver = new AppiumDriver<MobileElement>(new URL(urlDriver), device);
    }
}
