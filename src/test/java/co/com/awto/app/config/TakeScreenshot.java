package co.com.awto.app.config;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class TakeScreenshot {

    @Autowired
    private AppiumDriver<MobileElement> driver;

    @After
    public void TakeScreen(Scenario scenario) {
        try {
            if (scenario.isFailed()) {
                final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png", UUID.randomUUID().toString());
            }
        } catch (WebDriverException wde) {
            System.out.println("Hubo un error al tomar la captura de pantalla " + wde.getMessage());
        }
    }
}
