package co.com.awto.app.steps;

import co.com.awto.app.config.DriverConfig;
import co.com.awto.app.services.HomeServices;
import co.com.awto.app.services.MenuHomeServices;
import co.com.awto.app.services.MyAccountServices;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = {DriverConfig.class})
public class CucumberSpringConfiguration {

    @Autowired
    private HomeServices homeServices;
    @Autowired
    private MenuHomeServices menuHomeServices;
    @Autowired
    private MyAccountServices myAccountServices;

    @Before
    public void initContext() {
        System.out.println("Start context");
    }

    @After
    public void driverDown() throws Exception {
        myAccountServices.clickBackButton();
        homeServices.clickOnDrawerButton();
        menuHomeServices.clickOnLogOutOption();
        System.out.println("End Context");
    }
}
