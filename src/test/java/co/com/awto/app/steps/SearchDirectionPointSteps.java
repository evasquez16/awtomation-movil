package co.com.awto.app.steps;

import co.com.awto.app.services.HomeServices;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class SearchDirectionPointSteps {

    @Autowired
    private HomeServices homeServices;
    private String value;

    @When("^El realiza la busqueda del punto (.*)$")
    public void realizarBusquedaPunto(String punto) throws Exception {
        this.value = punto;
        homeServices.enterValueSearchDirectionPoint(punto);
    }

    @When("^El realiza la busqueda del direccion (.*)$")
    public void realizarBusquedaDireccion(String direccion) throws Exception {
        this.value = direccion;
        homeServices.enterValueSearchDirectionPoint(direccion);
    }

    @Then("^El ve en el listado (.*) esperado$")
    public void verResultadoListado(String typeSearch) throws Exception {
        Assert.assertTrue(homeServices.validateDirectionPointsPresent(value));
        homeServices.clickOnSearchPointDirectionOption(typeSearch, value);
    }

    @Then("^El no ve en el listado (.*) esperado$")
    public void noVeResultadoListado(String typeSearch) throws Exception {
        Assert.assertFalse(homeServices.validateDirectionPointsPresent(value));
    }
}
