package co.com.awto.app.steps;

import co.com.awto.app.services.HelpServices;
import co.com.awto.app.services.HomeServices;
import co.com.awto.app.services.MenuHomeServices;
import co.com.awto.app.services.MyAccountServices;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class HelpSteps {

    @Autowired
    private MenuHomeServices menuHome;
    @Autowired
    private HomeServices home;
    @Autowired
    private HelpServices help;
    @Autowired
    private MyAccountServices myAccount;

    @When("^El accede hasta el modulo de ayuda$")
    public void accedeModuloCodigoReferido() throws Exception {
        home.clickOnDrawerButton();
        menuHome.clickOnHelpOption();
    }

    @Then("^El valida llamado de emergencia a (.*)$")
    public void validateCallEmergency(String option) throws Exception {
        help.clickOnOption(option);
        help.clickOnAcceptPermission();
        help.clickOnCall();
        help.clickOnEndCall();
    }

    @Then("^El valida opcion de (.*)$")
    public void validateOptionAbout(String option) throws Exception {
        help.clickOnOption(option);
        help.clickOnDinamicOption(option);
        myAccount.clickBackButton();
    }
}
