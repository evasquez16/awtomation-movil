package co.com.awto.app.steps;

import co.com.awto.app.services.HomeServices;
import co.com.awto.app.services.MenuHomeServices;
import co.com.awto.app.services.PaysBillingServices;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class PaysBillingSteps {

    @Autowired
    private HomeServices homeServices;
    @Autowired
    private MenuHomeServices menuHomeServices;
    @Autowired
    private PaysBillingServices paysBillingServices;

    @When("^El accede hasta el formulario de pagos y facturacion$")
    public void elAccedeHastaElFormularioDePagosYFacturacion() throws Exception {
        homeServices.waitLoadingHomePage();
        homeServices.clickOnDrawerButton();
        menuHomeServices.clickOnPayVouchersOption();
    }

    @Then("^El ve el listado de Pagos y facturacion realizados$")
    public void elVeElListadoDePagosYFacturacion() throws Exception {
        Assert.assertEquals(paysBillingServices.getTitlePage(), "Pagos y facturaci\u00f3n");
        Assert.assertTrue(paysBillingServices.totalCredits());
        Assert.assertTrue(paysBillingServices.burdenTrip());
        Assert.assertTrue(paysBillingServices.paymentCredits());

    }
}
