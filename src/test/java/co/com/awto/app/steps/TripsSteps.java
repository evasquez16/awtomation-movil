package co.com.awto.app.steps;

import co.com.awto.app.services.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class TripsSteps {

    @Autowired
    private HomeServices homeServices;
    @Autowired
    private MyAccountServices myAccountServices;
    @Autowired
    private MenuHomeServices menuHomeServices;
    @Autowired
    private TripsServices tripsServices;
    @Autowired
    private TripDetailServices tripDetailServices;

    @When("El accede hasta el formulario de Viajes")
    public void accessTripsForm() throws Exception {
        homeServices.clickOnDrawerButton();
        menuHomeServices.clickOnTripsOption();
    }

    @Then("El ve el listado de Viajes realizados")
    public void viewTripsList() throws Exception {
        Assert.assertEquals(tripsServices.getTitlePage(), "Viajes");
        tripsServices.clickOnFirstTrip();
    }

    @And("^El ve los detalles del viaje realizado$")
    public void elSeleccionaYVeLosDetallesDelViajeRealizado() throws Exception {
        Assert.assertTrue(tripDetailServices.validateDetailPresent());
        myAccountServices.clickBackButton();
    }
}
