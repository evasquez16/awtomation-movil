package co.com.awto.app.steps;

import co.com.awto.app.services.Aw2youServices;
import co.com.awto.app.services.HomeServices;
import co.com.awto.app.services.MenuHomeServices;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class Aw2youSteps {

    @Autowired
    private MenuHomeServices menuHome;
    @Autowired
    private HomeServices home;
    @Autowired
    private Aw2youServices Aw2you;

    @When("^El accede hasta el modulo de Aw2you$")
    public void accedeModuloCallCenter() throws Exception {
        home.clickOnDrawerButton();
        menuHome.clickOnAwt2YouOption();
    }

    @Then("^El valida la llamada a Aw2you$")
    public void validateCallEmergency() throws Exception {
        Aw2you.clickOnOrderAw2you();
        Aw2you.clickOnAcceptPermission();
        Aw2you.clickOnCall();
        Aw2you.clickOnEndCall();
    }

}
