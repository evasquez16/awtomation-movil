package co.com.awto.app.steps;

import co.com.awto.app.services.HomeServices;
import co.com.awto.app.services.MenuHomeServices;
import co.com.awto.app.services.ReferredCodeServices;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class ReferredCodeSteps {

    @Autowired
    private MenuHomeServices menuHomeServices;
    @Autowired
    private HomeServices homeServices;
    @Autowired
    private ReferredCodeServices referredCodeServices;

    @When("^El accede hasta el modulo de codigo referido$")
    public void accedeModuloCodigoReferido() throws Exception {
        homeServices.clickOnDrawerButton();
        menuHomeServices.clickOnMyCodeOption();
    }

    @Then("^El valida que el codigo referido mostrado sea (.*)$")
    public void elValidaElCodigoReferidoMostrado(String code) throws Exception {
        Assert.assertEquals(referredCodeServices.getCodeReferred(), code);
    }

    @Then("^El valida la funcionalidad de copiar (.*)$")
    public void elValidaLaFuncionalidadDeLaOpcionCopiar(String value) throws Exception {
        referredCodeServices.clickOnCopyButton();
        homeServices.pasteValueSearchDirectionPoint(value);
    }

    @Then("El valida la funcionalidad de invitar amigos")
    public void elValidaLaFuncionalidadDeInvitarAmigos() throws Exception {
        Assert.assertTrue(referredCodeServices.validateMsgPresent());
        referredCodeServices.goToBack();
    }
}
