package co.com.awto.app.steps;

import co.com.awto.app.models.UserModel;
import co.com.awto.app.services.*;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class LoginSteps {

    @Autowired
    private HomeServices homeServices;
    @Autowired
    private MenuHomeServices menuHomeServices;
    @Autowired
    private WelcomeServices welcomeServices;
    @Autowired
    private LoginServices loginServices;
    @Autowired
    private PermissionLocationServices permissionLocationServices;
    @Autowired
    private MyAccountServices myAccountServices;
    @Autowired
    private UserModel userModel;

    @Given("^El ingresa a la App de Awto$")
    public void openAwtoApp(DataTable data) throws Exception {
        userModel.userModel(data.asList());
        //welcomeServices.waitForWelcomePage();
        welcomeServices.clickLoginButton();
        loginServices.enterUserAndPassword(userModel);
        permissionLocationServices.acceptPermissionLocation();
    }

    @Given("^El ingresa a la App de Awto con las nuevas credenciales$")
    public void accessToAwtoApp() throws Exception {
        welcomeServices.waitForWelcomePage();
        welcomeServices.clickLoginButton();
        loginServices.enterUserAndPassword(userModel);
    }

    @Then("^El puede ver la informacion del Usuario logueado$")
    public void iShouldSeeTheInfoAccountPage() throws Exception {
        homeServices.waitLoadingHomePage();
        homeServices.clickOnDrawerButton();
        menuHomeServices.clickOnMyAccount();
        Assert.assertTrue(myAccountServices.verifyUserLogin(userModel));
    }

    @And("^El ve el mensaje de cuenta en revision$")
    public void elVeElMensajeDeCuentaEnRevision() throws Exception {
        Assert.assertTrue(loginServices.viewMsgRevisionAccount());
        loginServices.clickOnButtonAcceptRevisionAccount();
    }
}