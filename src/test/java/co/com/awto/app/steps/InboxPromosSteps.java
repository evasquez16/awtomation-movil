package co.com.awto.app.steps;

import co.com.awto.app.services.HomeServices;
import co.com.awto.app.services.InboxPromosServices;
import co.com.awto.app.services.MenuHomeServices;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class InboxPromosSteps {

    @Autowired
    private HomeServices homeServices;
    @Autowired
    private MenuHomeServices menuHomeServices;
    @Autowired
    private InboxPromosServices inboxPromosServices;

    @When("^El accede hasta el formulario de Inbox y Promociones$")
    public void accedeFormularioInboxPromociones() throws Exception {
        homeServices.clickOnDrawerButton();
        menuHomeServices.clickOnInboxOption();
    }

    @Then("^El valida la seccion de Inbox$")
    public void elValidaLaSeccionDeInbox() throws Exception {
        inboxPromosServices.clickOnTabInbox();
        Assert.assertTrue(inboxPromosServices.msgNoItemsPresent());
    }

    @Then("^El valida la seccion de promociones$")
    public void elValidaLaSeccionDePromociones() throws Exception {
        inboxPromosServices.clickOnTabPromos();
        Assert.assertTrue(inboxPromosServices.msgNoItemsPresent());
    }

    @When("^El accede hasta el formulario de promociones$")
    public void elAccedeHastaElFormularioDePromociones() throws Exception {
        inboxPromosServices.clickOnTabPromos();
    }

    @And("^El registra una nueva promocion (.*): (.*)$")
    public void elRegistraUnaNuevaPromocionValida(String promoType, String codePromo) throws Exception {
        inboxPromosServices.enterValueCodePromo(codePromo);
    }

    @Then("^El ve el mensaje de promocion (.*)$")
    public void elVeElMensajeDePromocionRegistrada(String promoType) throws Exception {
        boolean msgPresent = inboxPromosServices.validateMessagePresent(promoType);
        if (!promoType.equals("valida") || !msgPresent)
            inboxPromosServices.goToBack();
        Assert.assertTrue(msgPresent);
    }
}
