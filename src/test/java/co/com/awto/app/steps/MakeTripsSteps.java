package co.com.awto.app.steps;

import co.com.awto.app.services.HomeServices;
import co.com.awto.app.services.MakeTripServices;
import co.com.awto.app.services.ReserveAwtoServices;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class MakeTripsSteps {

    @Autowired
    private HomeServices homeServices;
    @Autowired
    private ReserveAwtoServices reserveAwtoServices;
    @Autowired
    private MakeTripServices makeTripServices;

    @And("^El realiza la busqueda y selecciona (.*), (.*)$")
    public void searchSelectDirection(String typeSearch, String direccion) throws Exception {
        homeServices.enterValueSearchDirectionPoint(direccion);
        homeServices.clickOnSearchPointDirectionOption(typeSearch, direccion);
    }

    @When("^El selecciona (.*), selecciona el auto (.*) y la opcion reservar con el pin(.*)$")
    public void reservarAwto(String typeCar, String awto, String pin) throws Exception {
        homeServices.clickOnAwtoOption(typeCar);
        reserveAwtoServices.selectVehicle(awto);
        reserveAwtoServices.reserveVehicle(pin);
    }

    @When("^El selecciona (.*), selecciona el auto (.*) y la opcion reservar (.*) y cancela repetidamente el viaje$")
    public void reservarCalcenarAwto(String typeCar, String awto, String pin) throws Exception {
        for (int x = 1; x <= 3; x++) {
            reservarAwto(typeCar, awto, pin);
            if (x < 3)
                realizarCancelarVaije();
        }
    }

    @And("^El realiza y termina el viaje$")
    public void realizarTerminarVaije() throws Exception {
        makeTripServices.startEndTrip();
    }

    @And("^El realiza y cancela el vaije$")
    public void realizarCancelarVaije() throws Exception {
        makeTripServices.cancelTrip();
    }

    @Then("^El ve el HomePage sin reservas activas$")
    public void veHomePageSinReservas() throws Exception {
        Assert.assertTrue(reserveAwtoServices.verifyNotReserveActive());
    }

    @Then("^El ve el mensaje de cancelaciones multiples, cuenta suspendida$")
    public void mensajeCancelacionesMultiples() throws Exception {
        Assert.assertTrue(reserveAwtoServices.verifySuspendAccount());
    }

    @When("^El selecciona (.*), selecciona el auto (.*) y la opcion reserva extendida con el pin (.*)$")
    public void reservaExtendida(String typeCar, String awto, String pin) throws Exception {
        homeServices.clickOnAwtoOption(typeCar);
        reserveAwtoServices.selectVehicle(awto);
        reserveAwtoServices.selectExtendsReserve(pin);
    }

    @Then("^El ve el mensaje de saldo no disponible$")
    public void mensajeDeSaldoNoDisponible() throws Exception {
        Assert.assertTrue(reserveAwtoServices.verifyMessageNotBalance());
    }

    @And("^El inicia el viaje, carga combustible  y termina el viaje$")
    public void realizaViajeCargaCombustible() throws Exception {
        makeTripServices.onlyStartTrip();
        makeTripServices.rechargeFuel();
        makeTripServices.finishTrip();
        makeTripServices.sendExperience();
    }
}
