package co.com.awto.app.steps;

import co.com.awto.app.models.ClientModel;
import co.com.awto.app.models.UserModel;
import co.com.awto.app.services.*;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

public class ConfigureClientsSteps {

    @Autowired
    private WelcomeServices welcomeServices;
    @Autowired
    private CreateAccountServices createAccountServices;
    @Autowired
    private HomeServices homeServices;
    @Autowired
    private MenuHomeServices menuHomeServices;
    @Autowired
    private MyAccountServices myAccountServices;
    @Autowired
    private ClientModel clientModel;
    @Autowired
    private UserModel userModel;

    @Given("^El accede hasta el formulario de crear Cuenta$")
    public void elAccedeHastaElFormularioDeCrearCuenta() throws Exception {
        welcomeServices.clickCreateAccountButton();
    }

    @When("^El diligencia el formulario de crear cuenta$")
    public void elDiligenciaElFormularioDeCrearCuenta(DataTable data) throws Exception {
        clientModel.clientModel(data.asList());
        userModel.userModel(clientModel.getEmail(), clientModel.getPassword(), clientModel.getFullName());
        Assert.assertTrue(createAccountServices.verifySingUpImg());
        createAccountServices.registerClientFields(clientModel);
    }

    @Then("^El ve el mensaje confirmando la creacion del registro$")
    public void elVeElMensajeConfirmandoLaCreacionDelRegistro() throws Exception {
        Assert.assertTrue(createAccountServices.validateAccountCreate());
        createAccountServices.clickOnUnderstandButton();
    }

    @When("^El realiza el cambio de PIN de (.*) por (.*)$")
    public void realizaCambioPin(String oldPin, String newPin) throws Exception {
        homeServices.waitLoadingHomePage();
        homeServices.clickOnDrawerButton();
        menuHomeServices.clickOnMyAccount();
        myAccountServices.clickSettingPinButton();
        myAccountServices.enterUserPin(oldPin);
        myAccountServices.enterUserPin(newPin);
        myAccountServices.enterUserPin(newPin);
    }

    @When("^El realiza el cambio de password de (.*) por (.*)$")
    public void realizaCambioPassword(String oldPassword, String newPassword) throws Exception {
        homeServices.waitLoadingHomePage();
        homeServices.clickOnDrawerButton();
        menuHomeServices.clickOnMyAccount();
        myAccountServices.clickPasswordChangeButton();
        myAccountServices.enterOldPassword(oldPassword);
        myAccountServices.enterNewPassword(newPassword);
    }

    @Then("^El ve el mensaje de pin actualizado$")
    public void mensajePinActualizado() throws Exception {
        Assert.assertTrue(myAccountServices.msgPinUpdatePresent());
    }

    @Then("^El ve el mensaje de password actualizado$")
    public void mensajePasswordActualizado() throws Exception {
        Assert.assertTrue(myAccountServices.msgPasswordUpdatePresent());
    }
}
