package co.com.awto.app.steps;

import co.com.awto.app.services.*;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class CallCenterSteps {

    @Autowired
    private MenuHomeServices menuHome;
    @Autowired
    private HomeServices home;
    @Autowired
    private CallCenterServices callCenter;

    @When("^El accede hasta el modulo de Call Center$")
    public void accedeModuloCallCenter() throws Exception {
        home.clickOnDrawerButton();
        menuHome.clickOnCallCenterOption();
    }

    @Then("^El valida la llamada a CallCenter$")
    public void validateCallEmergency() throws Exception {
        callCenter.clickOnAcceptPermission();
        callCenter.clickOnCall();
        callCenter.clickOnEndCall();
    }

}
