package co.com.awto.app.runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/referredcode.feature",
        glue = {"co.com.awto.app"},
        monochrome = true,
        plugin = {"json:target/cucumber-reports/ReferredCode-report.json", "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"})
public class ReferredCodeRunner {

}
