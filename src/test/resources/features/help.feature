#Creado por Edison Vasquez Burbano

Feature: Revisar formulario de Help
  Como un cliente registrado
  Quiero usar el formulario Help
  Para validar la funcionalidad de Help

  Background: Logueo en App de Awto
    Given El ingresa a la App de Awto
      | victorhenriquez21@gmail.comtest | 123456 | Victor Henriquez |
    When El accede hasta el modulo de ayuda

  @ValidateEmergencyCall
  Scenario Outline: Validar opciones Asistencia Awto
    Then El valida llamado de emergencia a <emergency>

    Examples:
      | emergency  |
      | Ambulancia |
      | Bomberos   |

  @ValidateOptionsAbout
  Scenario Outline: Validar acerca de esta App
    Then El valida opcion de <about>

    Examples:
      | about                  |
      | Términos y Condiciones |
      | Tutorial Awto          |
     #| FAQ's                  |
