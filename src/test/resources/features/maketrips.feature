#Creado por Edison Vasquez Burbano
Feature: Validar funcionalidad de viajes
  Como un usuario registrado
  Quiero utilizar el módulo de viajes
  Para realizar viajes y cancelar reservas

  @MakeTrip
  Scenario Outline: Realizar reserva y viaje
    Given El ingresa a la App de Awto
      | <emailCliente> | <password> | <nombreCliente> |
    And El realiza la busqueda y selecciona la direccion, Pérez Valenzuela
    When El selecciona <tipoAuto>, selecciona el auto <auto> y la opcion reservar con el pin <pin>
    And El realiza y termina el viaje
    Then El ve los detalles del viaje realizado

    Examples:
      | emailCliente                | password | pin  | nombreCliente          | tipoAuto | auto   |
      | fern.galvan01@gmail.comtest | 123456   | 1234 | Fernando Galván Torres | Citycar  | LGYW18 |

  @ReserveCancelTrip
  Scenario Outline: Realizar reserva y cancelación
    Given El ingresa a la App de Awto
      | <emailCliente> | <password> | <nombreCliente> |
    When El selecciona <tipoAuto>, selecciona el auto <auto> y la opcion reservar con el pin <pin>
    And El realiza y cancela el vaije
    Then El ve el HomePage sin reservas activas

    Examples:
      | emailCliente                    | password | pin  | nombreCliente    | tipoAuto | auto   |
      | victorhenriquez21@gmail.comtest | 123456   | 1234 | Victor Henriquez | Citycar  | LGYW18 |

  @ReserveCancelRepeatTrip
  Scenario Outline: Realizar reserva y cancelación repeditamente
    Given El ingresa a la App de Awto
      | <emailCliente> | <password> | <nombreCliente> |
    When El selecciona <tipoAuto>, selecciona el auto <auto> y la opcion reservar <pin> y cancela repetidamente el viaje
    Then El ve el mensaje de cancelaciones multiples, cuenta suspendida

    Examples:
      | emailCliente                   | password | pin  | nombreCliente   | tipoAuto | auto   |
      | g.canalesnavarro@gmail.comtest | 123456   | 1234 | Gabriel Canales | Citycar  | LGYW18 |

  @ReserveExtendsTrip
  Scenario Outline: Realizar reserva extendida y viaje
    Given El ingresa a la App de Awto
      | <emailCliente> | <password> | <nombreCliente> |
    When El selecciona <tipoAuto>, selecciona el auto <auto> y la opcion reserva extendida con el pin <pin>
    And El realiza y termina el viaje
    Then El ve los detalles del viaje realizado

    Examples:
      | emailCliente               | password | pin  | nombreCliente | tipoAuto | auto   |
      | peter.garin@icloud.comtest | 123456   | 1234 | Peter Omar    | Citycar  | LGYW18 |

  @ReserveExtendsWithoutBalance
  Scenario Outline: Realizar reserva extendida sin saldo
    Given El ingresa a la App de Awto
      | <emailCliente> | <password> | <nombreCliente> |
    When El selecciona <tipoAuto>, selecciona el auto <auto> y la opcion reserva extendida con el pin <pin>
    Then El ve el mensaje de saldo no disponible

    Examples:
      | emailCliente                    | password | pin  | nombreCliente | tipoAuto | auto   |
      | luis.correa.leiva@gmail.comtest | 123456   | 1234 | Luis Felipe   | Citycar  | LGYW18 |

  @ReserveWithoutBalance
  Scenario Outline: Realizar reserva sin saldo
    Given El ingresa a la App de Awto
      | <emailCliente> | <password> | <nombreCliente> |
    When El selecciona <tipoAuto>, selecciona el auto <auto> y la opcion reservar con el pin <pin>
    Then El ve el mensaje de saldo no disponible

    Examples:
      | emailCliente                  | password | pin  | nombreCliente | tipoAuto | auto   |
      | javier.nercam@hotmail.comtest | 123456   | 1234 | Javier Nercam | Citycar  | LGYW18 |

  @ChargeFuel
  Scenario Outline: Realizar carga de bencina
    Given El ingresa a la App de Awto
      | <emailCliente> | <password> | <nombreCliente> |
    And El realiza la busqueda y selecciona la direccion, Pérez Valenzuela
    When El selecciona <tipoAuto>, selecciona el auto <auto> y la opcion reservar con el pin <pin>
    And El inicia el viaje, carga combustible  y termina el viaje
    Then El ve los detalles del viaje realizado

    Examples:
      | emailCliente                | password | pin  | nombreCliente | tipoAuto | auto   |
      | juanjosearcos@gmail.comtest | 123456   | 1234 | Juan Jose     | Citycar  | LGYW18 |

  @ChargeFuelRepeat
  Scenario Outline: Realizar carga de bencina repetidamente
    Given El ingresa a la App de Awto
      | <emailCliente> | <password> | <nombreCliente> |
    And El realiza la busqueda y selecciona la direccion, Pérez Valenzuela
    When El selecciona <tipoAuto>, selecciona el auto <auto> y la opcion reservar con el pin <pin>
    And El inicia el viaje, carga combustible  y termina el viaje
    Then El ve los detalles del viaje realizado

    Examples:
      | emailCliente                 | password | pin  | nombreCliente | tipoAuto | auto   |
      | rafagallogaete@gmail.comtest | 123456   | 1234 | Rafael        | Citycar  | LGYW18 |