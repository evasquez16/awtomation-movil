#Creado por Edison Vasquez Burbano

Feature: Revisar pagos y facturación
  Como un cliente registrado
  Quiero usar el formulario de pagos y facturación
  Para validar el listado de pagos y facturación

  Background: Logueo en App de Awto
    Given El ingresa a la App de Awto
      | florenciarama@gmail.comtest | 123456 | Florencia  Ramaciotti |

  @SearchPoints
  Scenario: Realizar búsqueda de puntos
    When El realiza la busqueda del punto Centro de Movilidad AWTO
    Then El ve en el listado el punto esperado

  @SearchDirection
  Scenario: Realizar búsqueda de direcciones
    When El realiza la busqueda del direccion Barrio Pehuen
    Then El ve en el listado la direccion esperado

  @SearchPointsNonExists
  Scenario: Realizar búsqueda de puntos no existentes
    When El realiza la busqueda del punto No existente
    Then El no ve en el listado el punto esperado

  @SearchDirectionNonExists
  Scenario: Realizar búsqueda de direcciones no existentes
    When El realiza la busqueda del direccion No existente
    Then El no ve en el listado la direccion esperado

