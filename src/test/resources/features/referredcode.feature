#Creado por Edison Vasquez Burbano

Feature: Revisar código de referido
  Como un cliente registrado
  Quiero usar el formulario de código referido
  Para validar la funcionalidad del formulario

  Background: Logueo en App de Awto
    Given El ingresa a la App de Awto
      | victorhenriquez21@gmail.comtest | 123456 | Victor Henriquez |
    When El accede hasta el modulo de codigo referido

  @ValidateReferredCode
  Scenario: Validar código referido mostrado
    Then El valida que el codigo referido mostrado sea NJ6RJFLU

  @ValidateCopyCode
  Scenario: Validar funcionalidad copiar
    Then El valida la funcionalidad de copiar NJ6RJFLU

  @ValidateInvitedFriends
  Scenario: Validar funcionalidad invitar amigos
    Then El valida la funcionalidad de invitar amigos