#Creado por Edison Vasquez Burbano

Feature: Revisar sección inbox y promos
  Como un cliente registrado
  Quiero usar el formulario de inbox y promos
  Para validar los inbox y la aplicación de promos

  Background: Logueo en App de Awto
    Given El ingresa a la App de Awto
      | peter.garin@icloud.comtest | 123456 | Peter Omar Garin |
     #| victorhenriquez21@gmail.comtest | 123456 | Victor Henriquez       |
     #| g.canalesnavarro@gmail.comtest  | 123456 | Gabriel Canales        |
     #| fern.galvan01@gmail.comtest     | 123456 | Fernando Galván Torres |
     #| luis.correa.leiva@gmail.comtest | 123456 | Luis Felipe            |
    When El accede hasta el formulario de Inbox y Promociones

  @ValidateInbox
  Scenario: Validar pestaña de Inbox
    Then El valida la seccion de Inbox

  @ValidatePromos
  Scenario: Validar pestaña de Promociones
    Then El valida la seccion de promociones


  @ValidatePromos
  Scenario: Registrar Promoción incorrecta
    And El accede hasta el formulario de promociones
    And El registra una nueva promocion incorrecta: INVALIDA123
    Then El ve el mensaje de promocion incorrecta

  @ValidatePromos
  Scenario: Registrar Promoción caducada
    And El accede hasta el formulario de promociones
    And El registra una nueva promocion caducada: PREFST2136MBJW8P
    Then El ve el mensaje de promocion caducada

  @ValidatePromos
  Scenario: Registrar Promoción repetida
    And El accede hasta el formulario de promociones
    And El registra una nueva promocion repetida: PREFST1737QNFE64
    Then El ve el mensaje de promocion repetida

  @ValidatePromos
  Scenario: Registrar Promoción válida solo registro
    And El accede hasta el formulario de promociones
    And El registra una nueva promocion solo registro: PREFST6394EMJ329
    Then El ve el mensaje de promocion solo registro

  @ValidatePromos
  Scenario: Registrar Promoción válida
    And El accede hasta el formulario de promociones
    And El registra una nueva promocion valida: CPMOV8I1EMV
    Then El ve el mensaje de promocion valida
