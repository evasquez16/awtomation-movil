#Creado por Edison Vasquez Burbano

Feature: Realizar registro de nuevo Cliente
  Como un nuevo cliente
  Quiero usar el formulario de registro
  Para realizar un nuevo registro

 @UpdatePinNumber
  Scenario Outline: Realizar cambio del PIN de reserva
    Given El ingresa a la App de Awto
      | <email> | <password> | <userName> |
    When El realiza el cambio de PIN de <oldPin> por <newPin>
    Then El ve el mensaje de pin actualizado

    Examples:
      | email            | password | userName       | oldPin | newPin |
      | evasquez@awto.cl | 123456   | Edison Vasquez | 1111   | 2222   |
      | evasquez@awto.cl | 123456   | Edison Vasquez | 2222   | 1111   |

  @UpdatePassword
  Scenario Outline: Realizar cambio del Password
    Given El ingresa a la App de Awto
      | <email> | <password> | <userName> |
    When El realiza el cambio de password de <oldPassword> por <newPassword>
    Then El ve el mensaje de password actualizado

    Examples:
      | email            | password | userName       | oldPassword | newPassword |
      | evasquez@awto.cl | 123456   | Edison Vasquez | 123456      | 654321      |
      | evasquez@awto.cl | 654321   | Edison Vasquez | 654321      | 123456      |


  @CreateNewClient
  Scenario Outline: Crear nuevo cliente
    When El accede hasta el formulario de crear Cuenta
    And El diligencia el formulario de crear cuenta
      | <name> | <lastName> | <email> | <password> | <gender> | <country> | <rut> | <direction> | <phoneNumber> | <typeVehicle> | <membership> | <codDiscount> | <optionPay> |
    Then El ve el mensaje confirmando la creacion del registro
    And El ingresa a la App de Awto con las nuevas credenciales
    And El ve el mensaje de cuenta en revision

    Examples:
      | name       | lastName | email   | password | gender    | country | rut    | direction        | phoneNumber | typeVehicle | membership | codDiscount | optionPay |
      | USARIO APP | AWTO     | userapp | 123456   | Masculino | Chile   | 123456 | Pérez Valenzuela | 85594       | Ambos       | Cero       | DISC123     | Ninguna   |

