#Creado por Edison Vasquez Burbano

Feature: Revisar pagos y facturación
  Como un cliente registrado
  Quiero usar el formulario de pagos y facturación
  Para validar el listado de pagos y facturación

  Background: Logueo en App de Awto
    Given El ingresa a la App de Awto
      | peter.garin@icloud.comtest | 123456 | Peter Omar Garin |

  @ValidateAccessPayBilling
  Scenario: Validar listado de pagos y facturación
    When El accede hasta el formulario de pagos y facturacion
    Then El ve el listado de Pagos y facturacion realizados