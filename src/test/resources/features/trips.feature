#Creado por Edison Vasquez Burbano

Feature: Revisar listado de Viajes
  Como un cliente registrado
  Quiero usar el formulario de Viajes
  Para validar el listado de viajes realizado

  @ValidateAccessTrips
  Scenario: Validar listado de viajes
    Given El ingresa a la App de Awto
      |  | 123456 | Florencia  Ramaciotti |
    When El accede hasta el formulario de Viajes
    Then El ve el listado de Viajes realizados
    And El ve los detalles del viaje realizado