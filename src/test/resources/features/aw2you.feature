#Creado por Edison Vasquez Burbano

Feature: Revisar llamada Aw2you
  Como un cliente registrado
  Quiero usar el menú de Aw2you
  Para validar la llamada a Aw2you

  Background: Logueo en App de Awto
    Given El ingresa a la App de Awto
      | victorhenriquez21@gmail.comtest | 123456 | Victor Henriquez |
    When El accede hasta el modulo de Aw2you

  @ValidateCallCenter
  Scenario: Validar llamada a Aw2you
    Then El valida la llamada a Aw2you