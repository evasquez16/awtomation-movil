#Creado por Edison Vasquez Burbano

Feature: Realizar el ingreso al sistema
  Como un cliente registrado
  Quiero usar el formulario de Login
  Para ingresar al sistema

  @ValidateAccessAwtoApp
  Scenario Outline: Realizar el ingreso con usuario válido
    Given El ingresa a la App de Awto
      | <email> | <password> | <userName> |
    Then El puede ver la informacion del Usuario logueado

    Examples:
      | email                       | password | userName              |
      | evasquez@awto.cl            | 123456   | Edison Vasquez        |
      | florenciarama@gmail.comtest | 123456   | Florencia  Ramaciotti |