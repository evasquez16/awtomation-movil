# Awtomation Movil

To Run the TestNG test on android
    
    * Platform="android" mvn clean -Dtest=Runner test

 To Run the TestNG test on iOS
  
    * Platform="ios" mvn clean -Dtest=Runner test

 To Run the TestNG test on iOS and android both in parallel
  
    * Platform="both" mvn clean -Dtest=Runner test
    
To Run the Cucumber test


    * mvn validate && Platform="android" mvn clean -Dtest=RunnerCukes test    